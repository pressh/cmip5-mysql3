#!/usr/bin/env python2

import sqlite3, sys
import os, datetime

class cmip5_dbase:
  """ Base class for CMIP5 database """
  def __init__(self,argv):
    self.dbase = argv
    self.initialize_database() # includes check for existing dbase

    self.filename="pylab.py"  
    
    self.connect_database()
    self.retrieve_from_database()
    self.close_database()
    
  def initialize_database(self):
    import logging.handlers # not sure if we can use this for backup purposes   
    if not os.path.exists(self.dbase): # create dbase
      self.create_database()
      self.connect_database()
      self.populate_database('/usr/people/haren/local/usr/lib64/python2.7')
      self.close_database()
    else: # do we need some backup solution
      pass
    
  def create_database(self):
    self.connect_database()
    self.cursor.execute('CREATE TABLE CMIP5 (Id INTEGER PRIMARY KEY, Date TEXT, Filename TEXT)')
    self.connection.commit()
    self.cursor.close()
    
  def connect_database(self):
    self.connection = sqlite3.connect(self.dbase)
    self.cursor = self.connection.cursor()
    
  def close_database(self):
    self.connection.commit()
    self.cursor.close()
  
  def add_to_database(self):
    self.cursor.execute('INSERT INTO CMIP5 VALUES(null, ?, ?)', (self.cr_date, self.filename))

  def retrieve_from_database(self):   
    self.cursor.execute('SELECT * FROM CMIP5 WHERE Filename=?' ,(self.filename,))
    self.all_entries = self.cursor.fetchone()
    print self.all_entries
      
  def delete_from_database(self):
    self.cursor.execute('DELETE FROM CMIP5 WHERE Filename=?', (self.filename,))  
    
  def update_entry_in_database(self):
    self.cursor.execute('UPDATE CMIP5 SET Date=? WHERE Filename=?' , ('newdate',self.filename,))
   
  def modification_date(self,filename):
    t = os.path.getmtime(filename)
    self.mod_date = datetime.datetime.fromtimestamp(t)

  def creation_date(self,filename):
    t = os.path.getctime(filename)
    self.cr_date = datetime.datetime.fromtimestamp(t)
    
  def populate_database(self,path):
    for root, dirs, files in os.walk(path):
      for self.filename in files:
	if self.filename.endswith(".py"):
	  self.modification_date(root+"/"+self.filename)
	  self.creation_date(root+"/"+self.filename)
	  self.add_to_database()
    
if __name__ == "__main__":
  dbase = "./test.db"
  cmip5_dbase(dbase)